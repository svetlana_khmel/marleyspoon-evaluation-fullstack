import React from 'react';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import withRedux from "next-redux-wrapper";
import store from '../redux/store';
import {Provider} from 'react-redux';
import Plans from '../components/Plans';
import Wrapper from '../styles/wrapper';
import { baseAPIURL } from '../API/api-settings';

const makeStore = () => store;

const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: baseAPIURL
});

const IndexComponent = () => (
  <ApolloProvider client={client}>
      <Provider store={store}>
    <Wrapper>
      <Plans />
    </Wrapper>
      </Provider>
  </ApolloProvider>
);

export default withRedux(makeStore)(IndexComponent);
