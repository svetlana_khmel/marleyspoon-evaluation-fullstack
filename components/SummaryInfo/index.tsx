import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Wrapper from './wrapper';
import { registerUser } from '../../redux/actions';
import Link from 'next/link';
import { IRootState } from '../../interfaces';

const SummaryInfo: React.FC = () => {
  const dispatch = useDispatch();
  const deliveryInformation = useSelector(
    (state: IRootState) => state.deliveryInformation
  );

  const register = () => {
        console.log('register');
        dispatch(registerUser());
  }

  const {
    address,
    city,
    country,
    deliveryDay,
    email,
    firstName,
    lastName
  } = deliveryInformation;
  return (
    <>
      <Wrapper>
        <div className="summary-info">
          <div className="label">Selected plan</div>
          <div className="value">{address}</div>
          <div className="label">Meals per week</div>
          <div className="value">{address}</div>
          <div className="label">Аddress</div>
          <div className="value">{address}</div>
          <div className="label">City</div>
          <div className="value">{city}</div>
          <div className="label">Country</div>
          <div className="value">{country}</div>
          <div className="label">Delivery Day</div>
          <div className="value">{deliveryDay}</div>
          <div className="label">Email</div>
          <div className="value">{email}</div>
          <div className="label">First Name</div>
          <div className="value">{firstName}</div>
          <div className="label">Last name</div>
          <div className="value">{lastName}</div>
        </div>
        <div className="navigation">
          <Link href={'/details'}>Back</Link>
          <div className="right">
            <div onClick={register} >Register</ div>
          </div>
        </div>
      </Wrapper>
    </>
  );
};

export default SummaryInfo;
