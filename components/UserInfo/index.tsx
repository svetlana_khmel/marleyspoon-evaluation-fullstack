import React, { FC, useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useQuery } from '@apollo/client';
import { useRouter } from 'next/router';
import Link from 'next/link';
import DatePicker from 'react-datepicker';
import { setUserInfo } from '../../redux/actions';
import { IRootState } from '../../interfaces';
import fieldValidation from '../../Utils/fieldValidation';
import 'react-datepicker/dist/react-datepicker.css';

import Wrapper from './wrapper';

const UserInfo: FC = () => {
  let inputValue = useSelector(
    (state: IRootState) => state.deliveryInformation
  );
  const [deliveryDate, setDeliveryDate] = useState(new Date());
  const [errors, setErrors] = useState({
    email: '',
    password: '',
    firstName: '',
    lastName: ''
  });

  const [isComplete, setIsComplete] = useState(false);
  const dispatch = useDispatch();
  const router = useRouter();
  const activePlan = router.query.plan;
  const activeOption = router.query.option;

  const handleChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
      const { name, value } = event.target;
      dispatch(
        setUserInfo({
          [name]: value,
          deliveryDate,
          activePlan,
          activeOption
        })
      );
      setErrors(fieldValidation(name, value, errors));
      setIsComplete(Object.values(errors).join("").length === 0 ? true : false);
    },[]);
  console.log('RERERE');
  return (
    <Wrapper>
      <div className="user-info">
        <h1>Enter your data and delivery preferences</h1>
      </div>
      <div className="options">
        <form>
          <fieldset>
            <label>Email</label>
            <input
              type="email"
              name="email"
              value={inputValue['email']}
              onChange={handleChange}
            />
            {errors.email.length > 0 && (
              <span className="error">{errors.email}</span>
            )}
          </fieldset>
          <fieldset>
            <label>Password</label>
            <input
              type="password"
              name="password"
              value={inputValue['password']}
              onChange={handleChange}
            />
            {errors.password.length > 0 && (
              <span className="error">{errors.password}</span>
            )}
          </fieldset>
          <fieldset>
            <h1>Delivery information</h1>
            <label>First name</label>
            <input
              type="text"
              name="firstName"
              value={inputValue['firstName']}
              onChange={handleChange}
            />
            {errors.firstName.length > 0 && (
              <span className="error">{errors.firstName}</span>
            )}
          </fieldset>
          <fieldset>
            <label>Last name</label>
            <input
              type="text"
              name="lastName"
              value={inputValue['lastName']}
              onChange={handleChange}
            />
            {errors.lastName.length > 0 && (
              <span className="error">{errors.lastName}</span>
            )}
          </fieldset>
          <fieldset>
            <label>Address</label>
            <input
              type="text"
              name="address"
              value={inputValue['address']}
              onChange={handleChange}
            />
          </fieldset>
          <fieldset>
            <label>City</label>
            <input
              type="text"
              name="city"
              value={inputValue['city']}
              onChange={handleChange}
            />
          </fieldset>
          <fieldset>
            <label>Country</label>
            <input
              type="text"
              name="country"
              value={inputValue['country']}
              onChange={handleChange}
            />
          </fieldset>
        </form>
        <div>
          <h3>Select delivery day:</h3>
          <DatePicker
            selected={deliveryDate}
            onChange={(date: any) => setDeliveryDate(date)}
          />
        </div>
        <div className="navigation">
          <Link href={'/'}>Back</Link>
          <div className="right">
                <Link href={'/summary'}>Summary</Link>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default UserInfo;
