const baseImgUrl = 'https://picsum.photos/';
const baseAPIURL = 'https://graphqlzero.almansi.me/api';
const baseGraphAPIURL = 'http://localhost:3302/api';

export {
    baseImgUrl,
    baseAPIURL,
    baseGraphAPIURL
}