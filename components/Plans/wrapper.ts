import styled from 'styled-components';
import * as palette from './variables'

const Wrapper = styled.div`
.plans {
    h1, div {
        text-align: center;
    }
}

.cards {
    display: grid;
    grid-template-columns: 1fr 1fr;
}

.options {
    width: 100px;
    margin: 0 auto;
    padding: 10px;
}
`;

export default Wrapper;
