import * as actionTypes from '../actions/actionTypes';

const initialState = {
    plans: [
        {
            2: 10.99,
            3: 9.49,
            4: 8.99,
            5: 8.99,
            6: 8.49
        },
        {
            2: 8.99,
            3: 7.99,
            4: 7.99,
            5: 7.49,
            6: 6.99
        }
    ],
    shipping: 8.99,
    choosenPlan: {
        mealsPerWeek: 2,
        perPortion: 10.99,
        vegeterian: false,
    },
    deliveryInformation: {
        email: '',
        password: '',
        firstName: '',
        lastName: '',
        address: '',
        city: '',
        country: '',
        deliveryDate: ''
    }
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_USER_INFO:
            const info = action.payload;
            return {...state, deliveryInformation: { ...state.deliveryInformation, ...info}};
        default:
            return {...state};
    }
};

export default rootReducer;