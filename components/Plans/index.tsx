import React from 'react';
import Wrapper from './wrapper';
import Card from '../Card';

const Plans: React.FC = () => {
  return (
    <>
      <Wrapper>
        <div className="plans">
          <h1>Select a plan:</h1>
          <div className="cards">
            <Card plan={0} />
            <Card plan={1} />
          </div>
        </div>
      </Wrapper>
    </>
  );
};

export default Plans;
