import styled from 'styled-components';

const Wrapper = styled.div`

    form {
        display: flex;
        flex-direction: row;
        div {
            font-family: Arial;
        }
    }
  
  .card {
    font-family: Arial;
    margin: 20px;
    
    h1 {
        font-size: 26px;
    }

    h2 {
        font-size: 26px;
    }
    
    p {
        font-size: 24px;
    }
  } 
`;

export default Wrapper;
