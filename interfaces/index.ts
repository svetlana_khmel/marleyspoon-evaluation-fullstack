import * as actionTypes from '../redux/actions/actionTypes';

interface IDeliveryInformation {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    address: string;
    city: string;
    country: string;
    deliveryDate: string;
}

interface IChoosenPlan {
    mealsPerWeek: number;
    perPortion: number;
    vegeterian: boolean
}

export interface IRootState {
    plans: any[];
    choosenPlan: IChoosenPlan;
    shipping: number;
    deliveryInformation: IDeliveryInformation
}

export interface IsetUserInfoAction {
    type: typeof actionTypes.SET_USER_INFO;
    payload: {};
}

//export type ActionTypes = SetTimelineAction | SetDetailsAction;
