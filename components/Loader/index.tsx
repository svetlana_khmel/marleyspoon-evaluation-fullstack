import React, { memo } from 'react';
import Wrapper from './wrapper';

const Loader: React.FC = () => (
  <Wrapper>
    <div className="lds-dual-ring"></div>
  </Wrapper>
);

export default memo(Loader);
