import React from 'react';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import withRedux from "next-redux-wrapper";
import store from '../redux/store';
import {Provider} from 'react-redux';
import UserInfo from '../components/UserInfo';
import Wrapper from '../styles/wrapper';
import { baseAPIURL } from '../API/api-settings';

const makeStore = () => store;

const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: baseAPIURL
});

const IndexComponent = () => (
    <ApolloProvider client={client}>
        <Wrapper>
            <Provider store={store}>
                <UserInfo />
            </Provider>
        </Wrapper>
    </ApolloProvider>
);

export default withRedux(makeStore)(IndexComponent);