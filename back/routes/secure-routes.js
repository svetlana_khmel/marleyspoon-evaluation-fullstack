const express = require('express');
const router = express.Router();
const Entry = require('../models/entry.js');
const UserModel = require('../models/model');
const fs = require('fs');

router.get('/', (request, response, next) => {
  response.send(request.user);
});

router.get('/profile', (req, res, next) => {
  res.json({
    message: 'You made it to the secure route',
    user: req.user,
    token: req.query.secret_token
  });
});

const sendData = (data, res) => {
  const json = JSON.parse(data);

  res.header('Content-Type', 'application/json');
  res.send(json);
}

router.get('/data', (req, res, next) => {

  // Entry.find()
  //   .then((result) => {
  //     console.log('Result get data ---- : ', result.data);
  //     res.json(result.data);
  //   })
  //   .catch((error) => {
  //     console.log('find ---- : ',error);
  //     res.status(500).json({ error });
  //   });

  Entry.find()
    .populate('user')
    .then((result) => {
      console.log('Result get data : ', result.data);
      res.json(result.data);
    })
    .catch((error) => {
      res.status(500).json({ error });
    });


  // next();

  // :: json ::
  // fs.readFile(`./data/${user}.json`, (err, data) => {
  //   if (err) {
  //     fs.readFile('./data/data', (err, data1) => {
  //       if (err) throw err;
  //       sendData(data, res);
  //     })
  //   }
  //   sendData(data, res);
  // });
});

const saveData = (data, user, callback) => {
  fs.writeFile(`./data/${user}.json`, JSON.stringify(data), callback);
};

router.post('/save', async (req, res, next) => {
  console.log('req . . . . . . . . . . .  . . . .', req.body);
  console.log('req . . . . . . . . . . .  . . . .', JSON.stringify(req.body));
  console.log('req . . . . . . . . . . .  .@@@@ . . .', JSON.parse(JSON.stringify(req.body)));
  const data = JSON.parse(JSON.stringify(req.body));



  let entry = await Entry.findOne().populate('user');

  const updatedEntry = await Entry.updateOne({}, { 'data': data });
  console.log('...updatedEntry...', updatedEntry);
  console.log('...updatedEntry entry...', entry);
  if (! entry ) {
    entry = new Entry({data: data});
    console.log('new entry : ', entry);
    entry.save((err, data) => {
      console.log('saved data ++', data);
      if (err) {
        res.status(500).send(err)
      } else {
        console.log('saved.... ++', data);
        res.status(200).json(data);
      };
    })
  } else {
    console.log('updated.... ++', updatedEntry);
    res.json(updatedEntry.data);
  }

});

module.exports = router;
