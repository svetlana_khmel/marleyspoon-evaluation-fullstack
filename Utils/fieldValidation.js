const fieldValidation = (name,  value, errors) => {
    let errorsSet = errors;
    switch (name) {
        case 'email':
            errorsSet.email =
                /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value)
                        ? ''
                        : 'Email is not valid!';
            break;
        case 'password':
             errorsSet.password =
                  value.length < 8
                    ? 'Password must be at least 8 characters long!'
                    : '';
             break;
        case 'firstName':
            errorsSet.firstName =
                 value.length < 5
                    ? 'Full Name must be at least 5 characters long!'
                    : '';
            break;
        case 'lastName':
            errorsSet.lastName =
                 value.length < 5
                    ? 'Full Name must be at least 5 characters long!'
                    : '';
       default:
            break;
    }
    return {...errors, [name]: errorsSet[name]};
}

export default fieldValidation;
