import React from 'react';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import withRedux from "next-redux-wrapper";
import store from '../redux/store';
import {Provider} from 'react-redux';
import SummaryInfo from '../components/SummaryInfo';
import Wrapper from '../styles/wrapper';
import { baseAPIURL } from '../API/api-settings';

const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: baseAPIURL
});

const IndexComponent = () => (
    <ApolloProvider client={client}>
        <Wrapper>
            <Provider store={store}>
                <SummaryInfo />
            </Provider>
        </Wrapper>
    </ApolloProvider>
);

export default IndexComponent;