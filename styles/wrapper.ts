import styled from 'styled-components';

const Wrapper = styled.div`
  .plans, .summary-info, .user-info {
    font-family: Arial;
  }
  
  .navigation .right {
    float: right;
  }
  
  .navigation a {
    font-size: 20px;
    text-decoration: none;
    background: chocolate;
    padding: 20px;
    color: #fff;
    font-family: Arial;
    width: 130px;
  }
`;

export default Wrapper;
