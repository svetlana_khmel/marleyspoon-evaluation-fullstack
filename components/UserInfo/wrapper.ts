import styled from 'styled-components';

const Wrapper = styled.div`
    .user-info, .options {
        font-family: Arial;
    }
    
    fieldset {
        border: 0;
    }
    
    label {
        width: 200px;
        display: block;
    }
    
    .navigation {
        margin-top: 50px;
        a {
            font-size: 20px;
            text-decoration: none;
            background: chocolate;
            padding: 20px;
            color: #fff;
            font-family: Arial;
            width: 130px;
        }
    }

    .error {
        color: red;
    }
`;

export default Wrapper;
