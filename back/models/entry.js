const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const EntrySchema = new Schema({
  data: {
    type: Array
  },
  _creator : { type: Schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Entry', EntrySchema);
