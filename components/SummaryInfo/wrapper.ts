import styled from 'styled-components';

const Wrapper = styled.div`

.summary-info {
    display: grid;
    grid-template-columns: 0.5fr 1fr;
}
.label, .value {
    font-size: 26px;
    font-family: Arial;
    padding: 20px; 
    border-bottom: 1px solid #333; 
}

.navigation {
    margin-top: 40px;

    a {
        font-size: 20px;
        text-decoration: none;
        background: chocolate;
        padding: 20px;
        color: #fff;
        font-family: Arial;
        width: 130px;
    }
}
`;

export default Wrapper;
