import React, { FC, useState } from 'react';
import { useSelector } from 'react-redux';
import Link from 'next/link';
import { IRootState } from '../../interfaces';

import Wrapper from './wrapper';

const Details: FC<number> = ({ plan }) => {
  const plans = useSelector((state: IRootState) => state.plans);
  const [checkedVal, setChecked] = useState<number>(2);

  const handleOptions = (event: React.ChangeEvent<HTMLInputElement>) =>
    setChecked(+event.target.value);

  const renderOption = () =>
    Object.keys(plans[plan]).map((option: string, i: number) => (
      <div key={option + i}>
        <input
          type="radio"
          value={option}
          name={option}
          checked={checkedVal === +option}
          onChange={handleOptions}
        />
      </div>
    ));

  return (
    <>
      <Wrapper>
        <div className="card">
          <h1>2 PEOPLE</h1>
          <h2>Delicious dinners perfect for couples</h2>
          <img src="https://picsum.photos/200" alt="" />
        </div>
        <div>Plan {plan + 1}</div>
        <div className="options">
          <form>{renderOption()}</form>
        </div>
        <div className="navigation">
          <div className="right">
            <Link href={`/details?plan=${plan}&option=${checkedVal}`}>
              Select
            </Link>
          </div>
        </div>
      </Wrapper>
    </>
  );
};

export default Details;
