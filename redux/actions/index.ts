import * as actionTypes from './actionTypes';
import { IsetUserInfoAction } from '../../interfaces';
import { baseGraphAPIURL } from '../../API/api-settings.ts'
import axios from 'axios';

const setUserInfo = (data: {}): IsetUserInfoAction => ({type: actionTypes.SET_USER_INFO, payload: data });

const registerUser = () => (dispatch, getState) => {
    const {
        email,
        password,
        address,
        mealsPerWeek,
        city,
        country,
        deliveryDay,
        lastName} = getState().deliveryInformation;

//         mutation register {
//           register(input: { email: "test@test.com", password: "password", name: "Jon Doe" }) {
//             id
//             name
//             email
//           }
//         }


        const name = getState().deliveryInformation.firstName;

        axios.post(baseGraphAPIURL, {
          query: `mutation register {
                    register(input: { email: ${email}, password: ${password}, name: ${name} }) {
                      id
                      name
                      email
                    }
                  }`,
          variables: {
            email,
            password,
            name
          }
        }, {
            headers: {
              'Content-Type': 'application/json'
            }
          })
}
export {
    setUserInfo,
    registerUser
};